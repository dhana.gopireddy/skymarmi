# Skymarmi

## Short Description of Sky marmi 
- Skymarmi offers six user types: Admin, Staff, Customer, Dealer, Store, and Security. Admins oversee all app activities, with access to monitoring and control features. Staff manage block and slab processes, while Customers and Dealers can browse the catalog, add items to cart, and place orders. The Store user handles chemical management, with options to add stock and view usage details. Security is responsible for adding visitor information. Each user type has specific features tailored to their roles, ensuring efficient operations within Skymarmi.

## User types with features

- Skymarmi has 6 types of logins they are 
Admin,
Staff,
Customer,
Dealer,
Store,
Security

## Admin Login features

- The admin can moniter all type of actions performed in the app  
- Admin Login is fixed to some users in the skymarmi and the developer .
- The Admin views and actions are controlling by deviceId 
- The buttons that admin can see in home page is 
Dashboard,
Visitors,
Reports,
Store details,
Users,
Add images

### Dashboard

- The dashboard button consists the complete data of blocks and slabs 
- The admin can see all the data in the app in this dashboard view with complete history 

### Visitors

- The visitors button consits the data of visitors of the company 
- the admin can see all the vistors by date ,which are added by the security 

### Reports 

- In reports the admin can see all the data which are present in the app in a graphical view 
- we are divided the all the reports into the categories like 

##### Block Reports 
- All type of Block reports can see in the Block Reports option  
- Blocks (Total ton present in each type of Blocks )
- Colour wise (Total ton present in blocks based on  colour wise)
- Country wise (Total ton present in blocksarrival based on  country wise)
- Stock in Hand ( currently the no of blocks are present in each process of blocks)
- Date wise reports ( How many blocks are processing in each process of blocks on date wise)
- Cutting Graph (Total ton present in the block cutting process )

##### Slab Reports
- All type of Slab Reports can see in th Slab reports option 
- Slabs (Total Sqft present in each type of Slab process)
- Colour wise (Total Sqft present in slab Process based on  colour wise)
- Country wise (Total Sqft present in slabs process based on  country wise)
- Stock in Hand ( currently the no of blocks are present in each slab process )
- Date wise reports ( How many blocks are processing in each slab process on date wise)
- epoxy Graph (Total Sqft present in the epoxy process )
- Pollishing Graph (Total Sqft present in the pollishing process )

##### Daily Reports 
- Daily Block Reports (How many blocks are processing in each process of blocks on today)
- Daily Slab Reports (How many blocks are processing in each process of slabs on today)

##### Monthly Reports 
- Area wise material sold report
- Monthly polishing done report

##### Loading Reports 
- Monthly Loading done report

##### Order Reports 
- Monthly Online Orders report (orders by Customer and Dealer login)
- Sold/unsold Status during blocks (staff or admin can gave sold status during the process of Blocks )
- Sold/unsold Status during Slabs (staff or admin can gave sold status during the process of Slabs )
- Sold/unsold Status during Loading (staff or admin can gave sold status during the process of Loading )

##### Visitors Report 
- Monthly Visitors count 

##### Attendance 
- how many workers are came for this day in a month 

##### Store
- Monthly chemical usage 
- Montly chemical ordered

### Store details 

- The store details button consists the data of ussage of chemicals that present in store 

### Users

- The Users button consits the data of all users of the app , the admin needs to approve them then only other users can see their respective views 

### Add images 

- Add images button is for adding the images for the blocks for first data that client has given , it can be also used for changing the images of blocks


## Staff login features 

- Staff can add the data 
- Staff needs the approval from admin for login to the app
- staff login consists these buttons 
Blocks,
Slabs,
Stacking,
Loading,
Recovery,
Visitor,
Add Images 

### Blocks
- The Blocks button consists the data of the blocks present in block process like (Block Arrival , Block Dressing , Block Reinforcement , Block Cutting )
- The staff can add the block details and they can change the blocks from to next process 

### Slabs 
- The Slabs button consists the data of the blocks present in slab process like (Unload of slabs , fiber ,Shifting slabs, Epoxy , Polishing , Stacking)
- The staff can change the blocks to next slab process

### Stacking 
- The Stacking button consists the details of blocks which are in stacking process 
- From the stacking the blocks will move to the loading process 
- the block details which are present in stacking can be seen also by Customers and Dealers in catalouge 

### Loading 
- Loading is the last one in slab process
- Loading button consists the details of blocks came to loading 

### Recovery 
- The Recovery button consists a form where the staff need to choose the block that are wasted during the process of blocks 
- In recovery process the staff needs to give the details like how much of Sqft is wasted for particular wasted block in Slab process 

### Visitor 
- The Staff can add the visitors those who are visited to the company 
- These details will shown to admin 

### Add Images 
- Add images button is for adding the images for the blocks for first data that client has given , it can be also used for changing the images of blocks

## Customer Login features
- Customer can see the data and able to order 
- Customer login needs approval from admin to see the catalouge 
- Customer login consits the these buttons 
Catalouge ,
Add2Cart,
MyOrders

### Catalouge 
- The Catalouge button consits the data of blocks that are there in stacking process 
- Customer can see the nessasary information of the block like name , colour , length , height , sqft 
- There is a option called add to cart , if customer likes that they can add that product to their cart

### Add2cart 
- The add2cart button consits the products data that he/she added in the cart 
- The customer choose the no of units and they can put order 

### MYOrders 
- The customer can see thier orders in Myorders button 


## Dealer Login features 
- Dealer can order behalf of his costumers 
- The Dealer can see the buttons like 
Catalouge,
Add2cart,
Myorders,
Customers

### Catalouge 
- The Catalouge button consits the data of blocks that are there in stacking process 
- Dealer can see the nessasary information of the block like name , colour , length , height , sqft 
- There is a option called add to cart , if customer likes that they can add that product to their cart

### Add2cart 
- The add2cart button consits the products data that he/she added in the cart 
- The Dealer choose the no of units and they can put order 

### MYOrders 
- The Dealers can see  thier orders in Myorders button with his customer details 

### Customers 
- The Dealer can see the his Customers list 


## Store Login features 
- Store user is one who manges the store and all chemicals in the store 
- Store login consists of 2 buttons they are 
Store Add ,
Store Detail

### Store Add 
- In store add button , the store person can add the chemicals in stock to the store 
- And they can also add the ordered chemical details 

### Store Detail 
- In store detail button , the store person can see the details of chemicals that he added 
- And he can see the ussage of chemicals in different stages of block and slab process by staff login 

## Security Login Features 
- The security has to add the visitors who are comming to visit the company 
- security has only one button   Visitors 
- where he can add the visitors details , and these details can seen by admin 


